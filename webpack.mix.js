const mix = require('laravel-mix');
const path = require('path');


mix.webpackConfig({
    resolve: {
      extensions: [".ts", ".scss", ".sass", ".tsx", ".js", ".jsx", ".vue", "*"],
      alias: {
        '@': path.resolve(__dirname, 'src'),
        '~': path.resolve(__dirname, 'node_modules')
      },
    },
  })
mix.setPublicPath('dist')
mix.disableNotifications()
mix.js('src/main.js', 'dist/js').vue()
  .version()
  .browserSync({
    proxy: false,
    ui: false
  })
mix.copyDirectory('public', 'dist')
