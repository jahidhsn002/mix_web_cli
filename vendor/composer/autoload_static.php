<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitc2dd3ea531e0b75b533bf0f9b1c57b89
{
    public static $files = array (
        '1a8e9e820738fd8a64e90335773ae9e1' => __DIR__ . '/../..' . '/mix.php',
    );

    public static $classMap = array (
        'Composer\\InstalledVersions' => __DIR__ . '/..' . '/composer/InstalledVersions.php',
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->classMap = ComposerStaticInitc2dd3ea531e0b75b533bf0f9b1c57b89::$classMap;

        }, null, ClassLoader::class);
    }
}
