<?php
// sitemap location
$sitemap_path = "sitemap.xml";

// URL Parser
$site_url_header = (isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] === 'on' ? "https" : "http");
$site_url = $site_url_header ."://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]";
$xml=(object) simplexml_load_file($sitemap_path);
$is_site_url_valid = false;
foreach($xml->url as $url) {
	if($url->loc == $site_url) {
		$is_site_url_valid = true; break;
	}
}

// if(!$is_site_url_valid) header("HTTP/2 404 Not Found"); // Or use HTTP/1.0 or HTTP 1.1
// else header('Content-Type: text/html; charset=utf-8');
