import { createApp } from 'vue'
import App from '@/App.vue'

import '@/styles/init.scss'
import router from '@/router'



const app = createApp(App)

// Mount Vue.js
app.use(router).mount('#app')

// Register Components
import ThemeDefault from '@/parts/ThemeDefault.vue'
import NotFound from '@/components/NotFound.vue'
	
app.component('theme-default', ThemeDefault)
app.component('not-found', NotFound)

export default app;