import { createRouter, createWebHistory } from 'vue-router'

import PageHome from '@/pages/PageHome.vue'
import PageArchive from '@/pages/PageArchive.vue'
import PageSingle from '@/pages/PageSingle.vue'
import PageNotFound from '@/pages/PageNotFound.vue'

const routes = [
  { path: '/', name: 'PageHome', component: PageHome },
  { path: '/:type/:slug', name: 'PageArchive', component: PageArchive },
  { path: '/:slug', name: 'PageSingle', component: PageSingle },
  { path: '/:pathMatch(.*)*', name: 'PageNotFound', component: PageNotFound }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export { routes }
export default router
